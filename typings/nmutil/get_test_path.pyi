from os import PathLike
from pathlib import Path
from typing import Any
import unittest


class RunCounter:
    def next(self, k: str) -> int:
        """get a incrementing run counter for a `str` key `k`. returns an `int`."""
        ...

    @staticmethod
    def get(obj: Any) -> RunCounter: ...


def get_test_path(test_case: unittest.TestCase,
                  base_path: str | PathLike[str]) -> Path:
    """get the `Path` for a particular unittest.TestCase instance
    (`test_case`). base_path is either a str or a path-like."""
    ...
