from typing import NoReturn, TypeVar

from typing_extensions import Literal, Self, final

_T_co = TypeVar("_T_co", covariant=True)
_T = TypeVar("_T")


# pyright currently doesn't like typing_extensions' definition
# -- added to typing in python 3.11
def assert_never(arg: NoReturn) -> NoReturn: ...


__all__ = [
    "assert_never",
    "final",
    "Literal",
    "Self",
]
