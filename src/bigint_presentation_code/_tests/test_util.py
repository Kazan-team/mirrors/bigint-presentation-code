import unittest

from bigint_presentation_code.util import BitSet


class TestBitSet(unittest.TestCase):
    def test_bitset_repr(self):
        self.assertEqual(repr(BitSet([*range(3, 20), 21, *range(23, 50)])),
                         "BitSet([*range(3, 20), 21, *range(23, 50)])")
        self.assertEqual(repr(BitSet(range(3, 20))), "BitSet(range(3, 20))")
        self.assertEqual(repr(BitSet([5, 10, 15, 20, 25, 30, 35])),
                         "BitSet(range(5, 40, 5))")
        self.assertEqual(repr(BitSet([5, 10, 15])), "BitSet([5, 10, 15])")
        self.assertEqual(repr(BitSet([5, 10, 15, 20])),
                         "BitSet(range(5, 25, 5))")
        self.assertEqual(repr(
            BitSet([*range(5, 8), *range(9, 12), 13, *range(14, 20, 2)])),
            "BitSet([*range(5, 8), *range(9, 12), 13, *range(14, 20, 2)])")
        self.assertEqual(repr(BitSet(bits=0xcccd)), "BitSet(bits=0xcccd)")
        self.assertEqual(repr(BitSet(bits=0xcccc)), "BitSet(bits=0xcccc)")
        self.assertEqual(repr(BitSet(bits=0x249249)),
                         "BitSet(range(0, 24, 3))")
        self.assertEqual(repr(BitSet(bits=0xaaaaaa)),
                         "BitSet(range(1, 25, 2))")
        self.assertEqual(repr(BitSet(bits=0x123456789abcdef)),
                         "BitSet(bits=0x123456789abcdef)")


if __name__ == "__main__":
    _ = unittest.main()
