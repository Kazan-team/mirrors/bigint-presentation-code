import sys
import unittest
from pathlib import Path

from bigint_presentation_code.compiler_ir import (Fn, GenAsmState, OpKind,
                                                  SSAVal)
from bigint_presentation_code.register_allocator import allocate_registers
from bigint_presentation_code.register_allocator_test_util import GraphDumper


class TestRegisterAllocator(unittest.TestCase):
    maxDiff = None

    def make_add_fn(self):
        # type: () -> tuple[Fn, SSAVal]
        fn = Fn()
        op0 = fn.append_new_op(OpKind.FuncArgR3, name="arg")
        arg = op0.outputs[0]
        MAXVL = 32
        op1 = fn.append_new_op(OpKind.SetVLI, immediates=[MAXVL], name="vl")
        vl = op1.outputs[0]
        op2 = fn.append_new_op(
            OpKind.SvLd, input_vals=[arg, vl], immediates=[0], maxvl=MAXVL,
            name="ld")
        a = op2.outputs[0]
        op3 = fn.append_new_op(OpKind.SvLI, input_vals=[vl], immediates=[0],
                               maxvl=MAXVL, name="li")
        b = op3.outputs[0]
        op4 = fn.append_new_op(OpKind.SetCA, name="ca")
        ca = op4.outputs[0]
        op5 = fn.append_new_op(
            OpKind.SvAddE, input_vals=[a, b, ca, vl], maxvl=MAXVL, name="add")
        s = op5.outputs[0]
        _ = fn.append_new_op(OpKind.SvStd, input_vals=[s, arg, vl],
                             immediates=[0], maxvl=MAXVL, name="st")
        return fn, arg

    def test_register_allocate(self):
        fn, _arg = self.make_add_fn()
        reg_assignments = allocate_registers(
            fn, debug_out=sys.stdout, dump_graph=GraphDumper(self))

        self.assertEqual(
            repr(reg_assignments),
            "{"
            "<add.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<add.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<st.inp0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<add.inp1.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<li.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<li.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<add.inp0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=78, reg_len=32), "
            "<ld.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<ld.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<arg.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<arg.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<ld.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<st.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<st.inp2.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<st.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ca.outputs[0]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<add.outputs[1]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<add.inp3.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<li.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<li.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ld.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ld.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<vl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1)"
            "}"
        )

    def test_gen_asm(self):
        fn, _arg = self.make_add_fn()
        reg_assignments = allocate_registers(
            fn, debug_out=sys.stdout, dump_graph=GraphDumper(self))

        self.assertEqual(
            repr(reg_assignments),
            "{"
            "<add.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<add.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<st.inp0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<add.inp1.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<li.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<li.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<add.inp0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=78, reg_len=32), "
            "<ld.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<ld.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<arg.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<arg.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<ld.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<st.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<st.inp2.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<st.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ca.outputs[0]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<add.outputs[1]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<add.inp3.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<li.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<li.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ld.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ld.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<vl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1)"
            "}"
        )
        state = GenAsmState(reg_assignments)
        fn.gen_asm(state)
        self.assertEqual(state.output, [
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.ld *14, 0(3)',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.addi *46, 0, 0',
            'setvl 0, 0, 32, 0, 1, 1',
            'subfc 0, 0, 0',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.or *78, *14, *14',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.adde *14, *78, *46',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.std *14, 0(3)'
        ])

    def test_register_allocate_graphs(self):
        fn, _arg = self.make_add_fn()
        graphs = {}  # type: dict[str, str]

        graph_dumper = GraphDumper(self)

        def dump_graph(name, dot):
            # type: (str, str) -> None
            self.assertNotIn(name, graphs, "duplicate graph name")
            graphs[name] = dot
            graph_dumper(name, dot)
        allocated = allocate_registers(
            fn, debug_out=sys.stdout, dump_graph=dump_graph)
        self.assertEqual(
            repr(allocated),
            "{"
            "<add.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<add.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<st.inp0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<add.inp1.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<li.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<li.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=32), "
            "<add.inp0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=78, reg_len=32), "
            "<ld.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<ld.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<arg.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<arg.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<ld.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<st.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<st.inp2.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<st.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ca.outputs[0]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<add.outputs[1]: <CA>>: "
            "Loc(kind=LocKind.CA, start=0, reg_len=1), "
            "<add.inp3.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<add.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<li.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<li.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ld.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<ld.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<vl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1)"
            "}"
        )
        # load expected graphs
        data_path = Path(__file__).with_suffix("")
        data_path /= "test_register_allocate_graphs"
        data_path /= "expected"
        expected_graphs = {}  # type: dict[str, str]
        expected_graph_names = [
            'initial',
            'step_0_simplify',
            'step_1_simplify',
            'step_2_simplify',
            'step_3_simplify',
            'step_4_simplify',
            'step_5_simplify',
            'step_6_simplify',
            'step_7_simplify',
            'step_8_simplify',
            'step_9_simplify',
            'step_10_simplify',
            'step_11_simplify',
            'step_12_copy_merge',
            'step_12_copy_merge_result',
            'step_13_copy_merge',
            'step_13_copy_merge_result',
            'step_14_copy_merge',
            'step_14_copy_merge_result',
            'step_15_copy_merge',
            'step_15_copy_merge_result',
            'step_16_simplify',
            'step_17_freeze',
            'step_18_freeze',
            'step_19_simplify',
            'step_20_copy_merge',
            'step_20_copy_merge_result',
            'step_21_copy_merge',
            'step_21_copy_merge_result',
            'step_22_simplify',
            'step_23_copy_merge',
            'step_23_copy_merge_result',
            'step_24_simplify',
            'final',
        ]
        for name in expected_graph_names:
            file_path = (data_path / name).with_suffix(".dot")
            expected_graphs[name] = file_path.read_text(encoding="utf-8")
        self.assertEqual(graphs, expected_graphs)

    def test_register_allocate_spread(self):
        fn = Fn()
        maxvl = 32
        vl = fn.append_new_op(OpKind.SetVLI, immediates=[maxvl],
                              name="vl", maxvl=maxvl).outputs[0]
        li = fn.append_new_op(OpKind.SvLI, input_vals=[vl], immediates=[0],
                              name="li", maxvl=maxvl).outputs[0]
        spread = fn.append_new_op(OpKind.Spread, input_vals=[li, vl],
                                  name="spread", maxvl=maxvl).outputs
        _concat = fn.append_new_op(
            OpKind.Concat, input_vals=[*spread[::-1], vl],
            name="concat", maxvl=maxvl)
        reg_assignments = allocate_registers(
            fn, debug_out=sys.stdout, dump_graph=GraphDumper(self))

        self.assertEqual(
            repr(reg_assignments),
            "{"
            "<spread.out31.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=1), "
            "<concat.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<li.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=45, reg_len=32), "
            "<li.out0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=45, reg_len=32), "
            "<spread.inp0.copy.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=45, reg_len=32), "
            "<spread.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=45, reg_len=1), "
            "<spread.outputs[1]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=46, reg_len=1), "
            "<spread.outputs[2]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=47, reg_len=1), "
            "<spread.outputs[3]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=48, reg_len=1), "
            "<spread.outputs[4]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=49, reg_len=1), "
            "<spread.outputs[5]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=50, reg_len=1), "
            "<spread.outputs[6]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=51, reg_len=1), "
            "<spread.outputs[7]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=52, reg_len=1), "
            "<spread.outputs[8]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=53, reg_len=1), "
            "<spread.outputs[9]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=54, reg_len=1), "
            "<spread.outputs[10]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=55, reg_len=1), "
            "<spread.outputs[11]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=56, reg_len=1), "
            "<spread.outputs[12]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=57, reg_len=1), "
            "<spread.outputs[13]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=58, reg_len=1), "
            "<spread.outputs[14]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=59, reg_len=1), "
            "<spread.outputs[15]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=60, reg_len=1), "
            "<spread.outputs[16]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=61, reg_len=1), "
            "<spread.outputs[17]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=62, reg_len=1), "
            "<spread.outputs[18]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=63, reg_len=1), "
            "<spread.outputs[19]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=64, reg_len=1), "
            "<spread.outputs[20]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=65, reg_len=1), "
            "<spread.outputs[21]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=66, reg_len=1), "
            "<spread.outputs[22]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=67, reg_len=1), "
            "<spread.outputs[23]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=68, reg_len=1), "
            "<spread.outputs[24]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=69, reg_len=1), "
            "<spread.outputs[25]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=70, reg_len=1), "
            "<spread.outputs[26]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=71, reg_len=1), "
            "<spread.outputs[27]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=72, reg_len=1), "
            "<spread.outputs[28]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=73, reg_len=1), "
            "<spread.outputs[29]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=74, reg_len=1), "
            "<spread.outputs[30]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=75, reg_len=1), "
            "<spread.outputs[31]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=76, reg_len=1), "
            "<concat.inp0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=1), "
            "<concat.inp1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=15, reg_len=1), "
            "<concat.inp2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=16, reg_len=1), "
            "<concat.inp3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=17, reg_len=1), "
            "<concat.inp4.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=18, reg_len=1), "
            "<concat.inp5.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=19, reg_len=1), "
            "<concat.inp6.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=20, reg_len=1), "
            "<concat.inp7.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=21, reg_len=1), "
            "<concat.inp8.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=22, reg_len=1), "
            "<concat.inp9.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=23, reg_len=1), "
            "<concat.inp10.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=24, reg_len=1), "
            "<concat.inp11.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=25, reg_len=1), "
            "<concat.inp12.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=26, reg_len=1), "
            "<concat.inp13.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=27, reg_len=1), "
            "<concat.inp14.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=28, reg_len=1), "
            "<concat.inp15.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=29, reg_len=1), "
            "<concat.inp16.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=30, reg_len=1), "
            "<concat.inp17.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=31, reg_len=1), "
            "<concat.inp18.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=32, reg_len=1), "
            "<concat.inp19.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=33, reg_len=1), "
            "<concat.inp20.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=34, reg_len=1), "
            "<concat.inp21.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=35, reg_len=1), "
            "<concat.inp22.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=36, reg_len=1), "
            "<concat.inp23.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=37, reg_len=1), "
            "<concat.inp24.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=38, reg_len=1), "
            "<concat.inp25.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=39, reg_len=1), "
            "<concat.inp26.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=40, reg_len=1), "
            "<concat.inp27.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=41, reg_len=1), "
            "<concat.inp28.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=42, reg_len=1), "
            "<concat.inp29.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=43, reg_len=1), "
            "<concat.inp30.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=44, reg_len=1), "
            "<concat.inp31.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=45, reg_len=1), "
            "<concat.outputs[0]: <I64*32>>: "
            "Loc(kind=LocKind.GPR, start=14, reg_len=32), "
            "<spread.out30.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=3, reg_len=1), "
            "<spread.out29.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=4, reg_len=1), "
            "<spread.out28.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=5, reg_len=1), "
            "<spread.out27.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=6, reg_len=1), "
            "<spread.out26.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=7, reg_len=1), "
            "<spread.out25.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=8, reg_len=1), "
            "<spread.out24.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=9, reg_len=1), "
            "<spread.out23.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=10, reg_len=1), "
            "<spread.out22.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=11, reg_len=1), "
            "<spread.out21.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=12, reg_len=1), "
            "<spread.out20.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=77, reg_len=1), "
            "<spread.out19.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=78, reg_len=1), "
            "<spread.out18.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=79, reg_len=1), "
            "<spread.out17.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=80, reg_len=1), "
            "<spread.out16.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=81, reg_len=1), "
            "<spread.out15.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=82, reg_len=1), "
            "<spread.out14.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=83, reg_len=1), "
            "<spread.out13.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=84, reg_len=1), "
            "<spread.out12.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=85, reg_len=1), "
            "<spread.out11.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=86, reg_len=1), "
            "<spread.out10.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=87, reg_len=1), "
            "<spread.out9.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=88, reg_len=1), "
            "<spread.out8.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=89, reg_len=1), "
            "<spread.out7.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=90, reg_len=1), "
            "<spread.out6.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=91, reg_len=1), "
            "<spread.out5.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=92, reg_len=1), "
            "<spread.out4.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=93, reg_len=1), "
            "<spread.out3.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=94, reg_len=1), "
            "<spread.out2.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=95, reg_len=1), "
            "<spread.out1.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=96, reg_len=1), "
            "<spread.out0.copy.outputs[0]: <I64>>: "
            "Loc(kind=LocKind.GPR, start=97, reg_len=1), "
            "<concat.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<concat.inp32.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<spread.inp1.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<spread.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<li.out0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<li.inp0.setvl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1), "
            "<vl.outputs[0]: <VL_MAXVL>>: "
            "Loc(kind=LocKind.VL_MAXVL, start=0, reg_len=1)"
            "}"
        )
        state = GenAsmState(reg_assignments)
        fn.gen_asm(state)
        self.assertEqual(state.output, [
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'sv.addi *45, 0, 0',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1',
            'or 97, 45, 45',
            'or 96, 46, 46',
            'or 95, 47, 47',
            'or 94, 48, 48',
            'or 93, 49, 49',
            'or 92, 50, 50',
            'or 91, 51, 51',
            'or 90, 52, 52',
            'or 89, 53, 53',
            'or 88, 54, 54',
            'or 87, 55, 55',
            'or 86, 56, 56',
            'or 85, 57, 57',
            'or 84, 58, 58',
            'or 83, 59, 59',
            'or 82, 60, 60',
            'or 81, 61, 61',
            'or 80, 62, 62',
            'or 79, 63, 63',
            'or 78, 64, 64',
            'or 77, 65, 65',
            'or 12, 66, 66',
            'or 11, 67, 67',
            'or 10, 68, 68',
            'or 9, 69, 69',
            'or 8, 70, 70',
            'or 7, 71, 71',
            'or 6, 72, 72',
            'or 5, 73, 73',
            'or 4, 74, 74',
            'or 3, 75, 75',
            'or 14, 76, 76',
            'or 15, 3, 3',
            'or 16, 4, 4',
            'or 17, 5, 5',
            'or 18, 6, 6',
            'or 19, 7, 7',
            'or 20, 8, 8',
            'or 21, 9, 9',
            'or 22, 10, 10',
            'or 23, 11, 11',
            'or 24, 12, 12',
            'or 25, 77, 77',
            'or 26, 78, 78',
            'or 27, 79, 79',
            'or 28, 80, 80',
            'or 29, 81, 81',
            'or 30, 82, 82',
            'or 31, 83, 83',
            'or 32, 84, 84',
            'or 33, 85, 85',
            'or 34, 86, 86',
            'or 35, 87, 87',
            'or 36, 88, 88',
            'or 37, 89, 89',
            'or 38, 90, 90',
            'or 39, 91, 91',
            'or 40, 92, 92',
            'or 41, 93, 93',
            'or 42, 94, 94',
            'or 43, 95, 95',
            'or 44, 96, 96',
            'or 45, 97, 97',
            'setvl 0, 0, 32, 0, 1, 1',
            'setvl 0, 0, 32, 0, 1, 1'
        ])


if __name__ == "__main__":
    _ = unittest.main()
