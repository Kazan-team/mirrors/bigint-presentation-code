import unittest
from fractions import Fraction

from bigint_presentation_code.matrix import Matrix, SpecialMatrix


class TestMatrix(unittest.TestCase):
    def test_repr(self):
        self.assertEqual(repr(Matrix(2, 3, [0, 1, 2,
                                            3, 4, 5])),
                         'Matrix(height=2, width=3, data=[\n'
                         '    0, 1, 2,\n'
                         '    3, 4, 5,\n'
                         '])')
        self.assertEqual(repr(Matrix(2, 3, [0, 1, Fraction(2) / 3,
                                            3, 4, 5])),
                         'Matrix(height=2, width=3, data=[\n'
                         '    0, 1, Fraction(2, 3),\n'
                         '    3, 4, 5,\n'
                         '])')
        self.assertEqual(repr(Matrix(0, 3)), 'Matrix(height=0, width=3)')
        self.assertEqual(repr(Matrix(2, 0)), 'Matrix(height=2, width=0)')

    def test_eq(self):
        self.assertFalse(Matrix(1, 1) == 5)
        self.assertFalse(5 == Matrix(1, 1))
        self.assertFalse(Matrix(2, 1) == Matrix(1, 1))
        self.assertFalse(Matrix(1, 2) == Matrix(1, 1))
        self.assertTrue(Matrix(1, 1) == Matrix(1, 1))
        self.assertTrue(Matrix(1, 1, [1]) == Matrix(1, 1, [1]))
        self.assertFalse(Matrix(1, 1, [2]) == Matrix(1, 1, [1]))

    def test_add(self):
        self.assertEqual(Matrix(2, 2, [1, 2, 3, 4])
                         + Matrix(2, 2, [40, 30, 20, 10]),
                         Matrix(2, 2, [41, 32, 23, 14]))

    def test_identity(self):
        self.assertEqual(Matrix(2, 2, data=SpecialMatrix.Identity),
                         Matrix(2, 2, [1, 0,
                                       0, 1]))
        self.assertEqual(Matrix(1, 3, data=SpecialMatrix.Identity),
                         Matrix(1, 3, [1, 0, 0]))
        self.assertEqual(Matrix(2, 3, data=SpecialMatrix.Identity),
                         Matrix(2, 3, [1, 0, 0,
                                       0, 1, 0]))
        self.assertEqual(Matrix(3, 3, data=SpecialMatrix.Identity),
                         Matrix(3, 3, [1, 0, 0,
                                       0, 1, 0,
                                       0, 0, 1]))

    def test_sub(self):
        self.assertEqual(Matrix(2, 2, [40, 30, 20, 10])
                         - Matrix(2, 2, [-1, -2, -3, -4]),
                         Matrix(2, 2, [41, 32, 23, 14]))

    def test_neg(self):
        self.assertEqual(-Matrix(2, 2, [40, 30, 20, 10]),
                         Matrix(2, 2, [-40, -30, -20, -10]))

    def test_mul(self):
        self.assertEqual(Matrix(2, 2, [1, 2, 3, 4]) * Fraction(3, 2),
                         Matrix(2, 2, [Fraction(3, 2), 3, Fraction(9, 2), 6]))
        self.assertEqual(Fraction(3, 2) * Matrix(2, 2, [1, 2, 3, 4]),
                         Matrix(2, 2, [Fraction(3, 2), 3, Fraction(9, 2), 6]))

    def test_matmul(self):
        self.assertEqual(Matrix(2, 2, [1, 2, 3, 4])
                         @ Matrix(2, 2, [4, 3, 2, 1]),
                         Matrix(2, 2, [8, 5, 20, 13]))
        self.assertEqual(Matrix(3, 2, [6, 5, 4, 3, 2, 1])
                         @ Matrix(2, 1, [1, 2]),
                         Matrix(3, 1, [16, 10, 4]))

    def test_inverse(self):
        self.assertEqual(Matrix(0, 0).inverse(), Matrix(0, 0))
        self.assertEqual(Matrix(1, 1, [2]).inverse(),
                         Matrix(1, 1, [Fraction(1, 2)]))
        self.assertEqual(Matrix(1, 1, [1]).inverse(),
                         Matrix(1, 1, [1]))
        self.assertEqual(Matrix(2, 2, [1, 0, 1, 1]).inverse(),
                         Matrix(2, 2, [1, 0, -1, 1]))
        self.assertEqual(Matrix(3, 3, [0, 1, 0,
                                       1, 0, 0,
                                       0, 0, 1]).inverse(),
                         Matrix(3, 3, [0, 1, 0,
                                       1, 0, 0,
                                       0, 0, 1]))
        _1_2 = Fraction(1, 2)
        _1_3 = Fraction(1, 3)
        _1_6 = Fraction(1, 6)
        self.assertEqual(Matrix(5, 5, [1, 0, 0, 0, 0,
                                       1, 1, 1, 1, 1,
                                       1, -1, 1, -1, 1,
                                       1, -2, 4, -8, 16,
                                       0, 0, 0, 0, 1]).inverse(),
                         Matrix(5, 5, [1, 0, 0, 0, 0,
                                       _1_2, _1_3, -1, _1_6, -2,
                                       -1, _1_2, _1_2, 0, -1,
                                       -_1_2, _1_6, _1_2, -_1_6, 2,
                                       0, 0, 0, 0, 1]))
        with self.assertRaisesRegex(ZeroDivisionError, "Matrix is singular"):
            _ = Matrix(1, 1, [0]).inverse()
        with self.assertRaisesRegex(ZeroDivisionError, "Matrix is singular"):
            _ = Matrix(2, 2, [0, 0, 1, 1]).inverse()
        with self.assertRaisesRegex(ZeroDivisionError, "Matrix is singular"):
            _ = Matrix(2, 2, [1, 0, 1, 0]).inverse()
        with self.assertRaisesRegex(ZeroDivisionError, "Matrix is singular"):
            _ = Matrix(2, 2, [1, 1, 1, 1]).inverse()


if __name__ == "__main__":
    _ = unittest.main()
