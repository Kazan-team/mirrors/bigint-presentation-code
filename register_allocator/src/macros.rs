macro_rules! validated_fields {
    (
        #[fields_ty = $fields_ty:ident $(, arbitrary = $Arbitrary:ident)? $(,)?]
        $(#[$meta:meta])*
        $vis:vis struct $ty:ident $body:tt
    ) => {
        $(#[$meta])*
        #[derive(::serde::Serialize, ::serde::Deserialize$(, $Arbitrary)?)]
        $vis struct $fields_ty $body

        $(#[$meta])*
        $vis struct $ty($fields_ty);

        $(impl<'a> $Arbitrary<'a> for $ty {
            fn arbitrary(u: &mut ::arbitrary::Unstructured<'a>) -> ::arbitrary::Result<Self> {
                Ok(<$fields_ty as ::arbitrary::Arbitrary>::arbitrary(u)?.try_into()?)
            }
            fn arbitrary_take_rest(u: ::arbitrary::Unstructured<'a>) -> ::arbitrary::Result<Self> {
                Ok(<$fields_ty as ::arbitrary::Arbitrary>::arbitrary_take_rest(u)?.try_into()?)
            }
            fn size_hint(depth: usize) -> (usize, Option<usize>) {
                <$fields_ty as ::arbitrary::Arbitrary>::size_hint(depth)
            }
        })?

        impl TryFrom<$fields_ty> for $ty {
            type Error = crate::error::Error;

            fn try_from(fields: $fields_ty) -> Result<Self, Self::Error> {
                $ty::new(fields)
            }
        }

        impl From<$ty> for $fields_ty {
            fn from(value: $ty) -> Self {
                value.0
            }
        }

        impl ::std::ops::Deref for $ty {
            type Target = $fields_ty;

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl $ty {
            $vis const fn get(&self) -> &$fields_ty {
                &self.0
            }
        }

        impl ::serde::Serialize for $ty {
            fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
            where
                S: ::serde::Serializer,
            {
                self.0.serialize(serializer)
            }
        }

        impl<'de> ::serde::Deserialize<'de> for $ty {
            fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
            where
                D: ::serde::Deserializer<'de>,
            {
                $fields_ty::deserialize(deserializer)?
                    .try_into()
                    .map_err(<D::Error as ::serde::de::Error>::custom)
            }
        }
    };
}

macro_rules! const_unwrap_opt {
    ($v:expr $(, $($msg:literal $(,)?)?)?) => {
        match $v {
            Some(v) => v,
            None => panic!(concat!("tried to unwrap None", $($(": ", $msg,)?)?)),
        }
    };
}

macro_rules! const_unwrap_res {
    ($v:expr $(, $($msg:literal $(,)?)?)?) => {
        match $v {
            Ok(v) => v,
            Err(_e) => panic!(concat!("tried to unwrap Err", $($(": ", $msg,)?)?)),
        }
    };
}

macro_rules! const_try {
    ($v:expr, Err($var:ident) => Err($err:expr) $(,)?) => {
        match $v {
            Ok(v) => v,
            Err($var) => return Err($err),
        }
    };
    ($v:expr $(,)?) => {
        match $v {
            Ok(v) => v,
            Err(e) => return Err(e),
        }
    };
}

macro_rules! const_try_opt {
    ($v:expr $(,)?) => {
        match $v {
            Some(v) => v,
            None => return None,
        }
    };
}

macro_rules! nzu32_lit {
    ($v:literal) => {{
        const V: ::std::num::NonZeroU32 = match ::std::num::NonZeroU32::new($v) {
            Some(v) => v,
            None => panic!("literal must not be zero"),
        };
        V
    }};
}
