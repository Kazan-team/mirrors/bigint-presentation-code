use crate::{
    error::{Error, Result},
    interned::{GlobalState, Intern, InternTarget, Interned},
    loc::{Loc, LocFields, LocKind, Ty},
};
use enum_map::{enum_map, EnumMap};
use num_bigint::BigUint;
use num_traits::{Num, Zero};
use once_cell::race::OnceBox;
use serde::{de, Deserialize, Serialize};
use std::{
    borrow::{Borrow, Cow},
    cell::Cell,
    collections::{
        btree_map::{self, Entry},
        BTreeMap,
    },
    fmt,
    hash::Hash,
    iter::FusedIterator,
    mem,
    num::NonZeroU32,
    ops::{BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Range, Sub, SubAssign},
};

#[inline]
fn zero_biguint<'a>() -> &'a BigUint {
    static ZERO: OnceBox<BigUint> = OnceBox::new();
    ZERO.get_or_init(
        #[cold]
        || BigUint::zero().into(),
    )
}

#[derive(Debug, Clone)]
struct BigUintAsHexString<'a>(Cow<'a, BigUint>);

impl<'de, 'a> Deserialize<'de> for BigUintAsHexString<'a> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let text = String::deserialize(deserializer)?;
        if let Some(text) = text.strip_prefix("0x") {
            Ok(Self(Cow::Owned(
                BigUint::from_str_radix(text, 0x10).map_err(<D::Error as de::Error>::custom)?,
            )))
        } else {
            Err(<D::Error as de::Error>::custom(
                "expected hex string to start with `0x`",
            ))
        }
    }
}

impl Serialize for BigUintAsHexString<'_> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        format!("{:#x}", &*self.0).serialize(serializer)
    }
}

#[derive(Deserialize, Serialize)]
struct LocSetSerialized<'a> {
    starts_map: BTreeMap<NonZeroU32, BTreeMap<LocKind, BigUintAsHexString<'a>>>,
}

impl TryFrom<LocSetSerialized<'_>> for LocSet {
    type Error = Error;

    fn try_from(value: LocSetSerialized) -> Result<Self, Self::Error> {
        Self::from_starts_map(
            value
                .starts_map
                .into_iter()
                .map(|(k, mut v)| {
                    let v = enum_map! {
                        k => v.remove(&k).map_or_else(BigUint::zero, |v| v.0.into_owned())
                    };
                    (k, v)
                })
                .collect(),
        )
    }
}

#[derive(Clone, Default, PartialEq, Eq, Hash, Deserialize)]
#[serde(try_from = "LocSetSerialized")]
pub struct LocSet {
    starts_map: BTreeMap<NonZeroU32, EnumMap<LocKind, BigUint>>,
}

impl Serialize for LocSet {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        LocSetSerialized {
            starts_map: self
                .starts_map
                .iter()
                .map(|(&k, v)| {
                    let v = v
                        .iter()
                        .map(|(k, v)| (k, BigUintAsHexString(Cow::Borrowed(v))))
                        .collect::<BTreeMap<_, _>>();
                    (k, v)
                })
                .collect(),
        }
        .serialize(serializer)
    }
}

impl<'a> arbitrary::Arbitrary<'a> for LocSet {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        u.arbitrary_iter()?.collect()
    }
}

/// computes same value as `a & !b`, but more efficiently
fn and_not<A: Borrow<BigUint>, B>(a: A, b: B) -> BigUint
where
    BigUint: for<'a> BitXor<A, Output = BigUint>,
    B: for<'a> BitAnd<&'a BigUint, Output = BigUint>,
{
    // use logical equivalent that avoids needing to use BigInt
    (b & a.borrow()) ^ a
}

impl From<Loc> for LocSet {
    fn from(value: Loc) -> Self {
        Self::from_iter([value])
    }
}

impl LocSet {
    pub fn arbitrary_with_ty(
        ty: Ty,
        u: &mut arbitrary::Unstructured<'_>,
    ) -> arbitrary::Result<Self> {
        let kinds = ty.base_ty.loc_kinds();
        type Mask = u128;
        let kinds: Vec<_> = if kinds.len() > Mask::BITS as usize {
            let chosen_kinds = kinds
                .iter()
                .zip(u.arbitrary_iter::<bool>()?)
                .filter(|(_, cond)| !matches!(cond, Ok(false)))
                .map(|(&kind, cond)| {
                    cond?;
                    Ok(kind)
                })
                .collect::<arbitrary::Result<Vec<_>>>()?;
            if chosen_kinds.is_empty() {
                vec![*u.choose(kinds)?]
            } else {
                chosen_kinds
            }
        } else {
            let max_mask = Mask::wrapping_shl(1, kinds.len() as u32).wrapping_sub(1);
            let mask = u.int_in_range(1..=max_mask)?; // non-zero
            kinds
                .iter()
                .enumerate()
                .filter_map(|(idx, &kind)| {
                    if mask & (1 << idx) != 0 {
                        Some(kind)
                    } else {
                        None
                    }
                })
                .collect()
        };
        let mut starts = EnumMap::<LocKind, BigUint>::default();
        let mut all_zero = true;
        for kind in kinds {
            let bit_count = Loc::max_start(kind, ty.reg_len)? + 1;
            let byte_count = (bit_count + u8::BITS - 1) / u8::BITS;
            let bytes = u.bytes(byte_count as usize)?;
            starts[kind] = BigUint::from_bytes_le(bytes);
            starts[kind] &= (BigUint::from(1u8) << bit_count) - 1u8;
            all_zero &= starts[kind].is_zero();
        }
        if all_zero {
            Ok(Loc::arbitrary_with_ty(ty, u)?.into())
        } else {
            Ok(Self::from_starts_map_iter_unchecked([(ty.reg_len, starts)]))
        }
    }
    pub fn starts(&self, reg_len: NonZeroU32, kind: LocKind) -> &BigUint {
        self.starts_map
            .get(&reg_len)
            .map(|v| &v[kind])
            .unwrap_or_else(zero_biguint)
    }
    pub fn stops(&self, reg_len: NonZeroU32, kind: LocKind) -> BigUint {
        self.starts(reg_len, kind) << reg_len.get()
    }
    pub fn starts_map(&self) -> &BTreeMap<NonZeroU32, EnumMap<LocKind, BigUint>> {
        &self.starts_map
    }
    pub const fn new() -> Self {
        Self {
            starts_map: BTreeMap::new(),
        }
    }
    /// filters out empty entries, but doesn't do any other checks
    fn from_starts_map_iter_unchecked(
        starts_map: impl IntoIterator<Item = (NonZeroU32, EnumMap<LocKind, BigUint>)>,
    ) -> Self {
        Self {
            starts_map: starts_map
                .into_iter()
                .filter(|(_, starts)| !starts.iter().all(|(_, starts)| starts.is_zero()))
                .collect(),
        }
    }
    fn for_each_reg_len_filtering_out_empty_entries(
        &mut self,
        mut f: impl FnMut(NonZeroU32, &mut EnumMap<LocKind, BigUint>),
    ) {
        self.starts_map.retain(|&reg_len, starts| {
            f(reg_len, starts);
            !Self::is_entry_empty(starts)
        });
    }
    /// helper for binary operations that keeps Locs not present in rhs
    fn bin_op_keep_helper(
        &mut self,
        rhs: &Self,
        mut f: impl FnMut(NonZeroU32, &mut EnumMap<LocKind, BigUint>, &EnumMap<LocKind, BigUint>),
    ) {
        rhs.starts_map.iter().for_each(|(&reg_len, rhs_starts)| {
            match self.starts_map.entry(reg_len) {
                Entry::Vacant(entry) => {
                    let mut lhs_starts = EnumMap::default();
                    f(reg_len, &mut lhs_starts, rhs_starts);
                    if !Self::is_entry_empty(&lhs_starts) {
                        entry.insert(lhs_starts);
                    }
                }
                Entry::Occupied(mut entry) => {
                    f(reg_len, entry.get_mut(), rhs_starts);
                    if Self::is_entry_empty(entry.get()) {
                        entry.remove();
                    }
                }
            }
        });
    }
    fn is_entry_empty(starts: &EnumMap<LocKind, BigUint>) -> bool {
        starts.iter().all(|(_, starts)| starts.is_zero())
    }
    pub fn from_starts_map(
        mut starts_map: BTreeMap<NonZeroU32, EnumMap<LocKind, BigUint>>,
    ) -> Result<Self> {
        let mut error = Ok(());
        starts_map.retain(|&reg_len, starts| {
            if error.is_err() {
                return false;
            }
            let mut any_locs = false;
            for (kind, starts) in starts {
                if !starts.is_zero() {
                    any_locs = true;
                    error = (|| {
                        // bits() is one past max bit set, so use >= rather than >
                        if starts.bits() >= Loc::max_start(kind, reg_len)? as u64 {
                            return Err(Error::StartNotInValidRange);
                        }
                        Ok(())
                    })();
                    if error.is_err() {
                        return false;
                    }
                }
            }
            any_locs
        });
        Ok(Self { starts_map })
    }
    pub fn clear(&mut self) {
        self.starts_map.clear();
    }
    pub fn contains_exact(&self, value: Loc) -> bool {
        self.starts(value.reg_len, value.kind).bit(value.start as _)
    }
    pub fn insert(&mut self, value: Loc) -> bool {
        let starts = match self.starts_map.entry(value.reg_len) {
            Entry::Occupied(entry) => entry.into_mut(),
            Entry::Vacant(entry) => {
                entry.insert(Default::default())[value.kind].set_bit(value.start as u64, true);
                return true;
            }
        };
        let starts = &mut starts[value.kind];
        let retval = !starts.bit(value.start as u64);
        starts.set_bit(value.start as u64, true);
        retval
    }
    pub fn remove(&mut self, value: Loc) -> bool {
        let Entry::Occupied(mut entry) = self.starts_map.entry(value.reg_len) else {
            return false;
        };
        let starts = entry.get_mut();
        if starts[value.kind].bit(value.start as u64) {
            starts[value.kind].set_bit(value.start as u64, false);
            if starts.values().all(BigUint::is_zero) {
                entry.remove();
            }
            true
        } else {
            false
        }
    }
    pub fn is_empty(&self) -> bool {
        self.starts_map.is_empty()
    }
    pub fn iter(&self) -> Iter<'_> {
        Iter {
            internals: IterInternals::new(self.starts_map.iter()),
        }
    }
    pub fn len(&self) -> usize {
        let retval: u64 = self
            .starts_map
            .values()
            .map(|starts| starts.values().map(BigUint::count_ones).sum::<u64>())
            .sum();
        retval as usize
    }
    /// computes `self = &other - &self`
    pub fn sub_reverse_assign(&mut self, other: impl Borrow<Self>) {
        // TODO: make more efficient
        let other: &Self = other.borrow();
        *self = other - &*self;
    }
}

#[derive(Clone, Debug)]
struct IterInternalsRest<StartsMapValueIter, Starts>
where
    StartsMapValueIter: Iterator<Item = (LocKind, Starts)>,
    Starts: Borrow<BigUint>,
{
    reg_len: NonZeroU32,
    starts_map_value_iter: StartsMapValueIter,
    kind: LocKind,
    starts: Starts,
    start_range: Range<u32>,
}

impl<StartsMapValueIter, Starts> IterInternalsRest<StartsMapValueIter, Starts>
where
    StartsMapValueIter: Iterator<Item = (LocKind, Starts)>,
    Starts: Borrow<BigUint>,
{
    fn new(reg_len: NonZeroU32, mut starts_map_value_iter: StartsMapValueIter) -> Option<Self> {
        loop {
            let (kind, starts) = starts_map_value_iter.next()?;
            let starts_ref: &BigUint = starts.borrow();
            let Some(start) = starts_ref.trailing_zeros() else {
                continue;
            };
            let start = start.try_into().expect("checked by LocSet constructors");
            let end = starts_ref
                .bits()
                .try_into()
                .expect("checked by LocSet constructors");
            return Some(Self {
                reg_len,
                starts_map_value_iter,
                kind,
                starts,
                start_range: start..end,
            });
        }
    }
    fn next(this: &mut Option<Self>) -> Option<Loc> {
        while let Some(Self {
            reg_len,
            starts_map_value_iter: _,
            kind,
            ref starts,
            ref mut start_range,
        }) = *this
        {
            let Some(start) = start_range.next() else {
                *this = Self::new(reg_len, this.take().expect("known to be Some").starts_map_value_iter);
                continue;
            };
            if starts.borrow().bit(start as u64) {
                return Some(
                    Loc::new(LocFields {
                        kind,
                        start,
                        reg_len,
                    })
                    .expect("known to be valid"),
                );
            }
        }
        None
    }
}

#[derive(Clone, Debug)]
struct IterInternals<StartsMapIter, RegLen, StartsMapValue, StartsMapValueIter, Starts>
where
    StartsMapIter: Iterator<Item = (RegLen, StartsMapValue)>,
    RegLen: Borrow<NonZeroU32>,
    StartsMapValue: IntoIterator<IntoIter = StartsMapValueIter>,
    StartsMapValueIter: Iterator<Item = (LocKind, Starts)>,
    Starts: Borrow<BigUint>,
{
    starts_map_iter: StartsMapIter,
    rest: Option<IterInternalsRest<StartsMapValueIter, Starts>>,
}

impl<StartsMapIter, RegLen, StartsMapValue, StartsMapValueIter, Starts>
    IterInternals<StartsMapIter, RegLen, StartsMapValue, StartsMapValueIter, Starts>
where
    StartsMapIter: Iterator<Item = (RegLen, StartsMapValue)>,
    RegLen: Borrow<NonZeroU32>,
    StartsMapValue: IntoIterator<IntoIter = StartsMapValueIter>,
    StartsMapValueIter: Iterator<Item = (LocKind, Starts)>,
    Starts: Borrow<BigUint>,
{
    fn new(starts_map_iter: StartsMapIter) -> Self {
        Self {
            starts_map_iter,
            rest: None,
        }
    }
    fn next(&mut self) -> Option<Loc> {
        loop {
            while self.rest.is_none() {
                let (reg_len, starts_map_value) = self.starts_map_iter.next()?;
                self.rest = IterInternalsRest::new(*reg_len.borrow(), starts_map_value.into_iter());
            }
            if let Some(loc) = IterInternalsRest::next(&mut self.rest) {
                return Some(loc);
            }
        }
    }
}

#[derive(Clone, Debug)]
pub struct Iter<'a> {
    internals: IterInternals<
        btree_map::Iter<'a, NonZeroU32, EnumMap<LocKind, BigUint>>,
        &'a NonZeroU32,
        &'a EnumMap<LocKind, BigUint>,
        enum_map::Iter<'a, LocKind, BigUint>,
        &'a BigUint,
    >,
}

impl Iterator for Iter<'_> {
    type Item = Loc;

    fn next(&mut self) -> Option<Self::Item> {
        self.internals.next()
    }
}

impl FusedIterator for Iter<'_> {}

pub struct IntoIter {
    internals: IterInternals<
        btree_map::IntoIter<NonZeroU32, EnumMap<LocKind, BigUint>>,
        NonZeroU32,
        EnumMap<LocKind, BigUint>,
        enum_map::IntoIter<LocKind, BigUint>,
        BigUint,
    >,
}

impl Iterator for IntoIter {
    type Item = Loc;

    fn next(&mut self) -> Option<Self::Item> {
        self.internals.next()
    }
}

impl FusedIterator for IntoIter {}

impl IntoIterator for LocSet {
    type Item = Loc;
    type IntoIter = IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            internals: IterInternals::new(self.starts_map.into_iter()),
        }
    }
}

impl<'a> IntoIterator for &'a LocSet {
    type Item = Loc;
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl Extend<Loc> for LocSet {
    fn extend<T: IntoIterator<Item = Loc>>(&mut self, iter: T) {
        iter.into_iter().for_each(|item| {
            self.insert(item);
        });
    }
}

impl FromIterator<Loc> for LocSet {
    fn from_iter<T: IntoIterator<Item = Loc>>(iter: T) -> Self {
        let mut retval = LocSet::new();
        retval.extend(iter);
        retval
    }
}

struct HexBigUint<'a>(&'a BigUint);

impl fmt::Debug for HexBigUint<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#x}", self.0)
    }
}

struct LocSetStarts<'a>(&'a EnumMap<LocKind, BigUint>);

impl fmt::Debug for LocSetStarts<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_map()
            .entries(self.0.iter().map(|(k, v)| (k, HexBigUint(v))))
            .finish()
    }
}

struct LocSetStartsMap<'a>(&'a BTreeMap<NonZeroU32, EnumMap<LocKind, BigUint>>);

impl fmt::Debug for LocSetStartsMap<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_map()
            .entries(self.0.iter().map(|(k, v)| (k, LocSetStarts(v))))
            .finish()
    }
}

impl fmt::Debug for LocSet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("LocSet")
            .field("starts_map", &LocSetStartsMap(&self.starts_map))
            .finish()
    }
}

macro_rules! forward_bin_op {
    (
        $bin_op:ident::$bin_op_fn:ident(),
        $bin_assign_op:ident::$bin_assign_op_fn:ident(),
        $bin_assign_rev_op_fn:ident(),
    ) => {
        impl $bin_assign_op<LocSet> for LocSet {
            fn $bin_assign_op_fn(&mut self, rhs: LocSet) {
                self.$bin_assign_op_fn(&rhs);
            }
        }

        impl $bin_op<&'_ LocSet> for LocSet {
            type Output = LocSet;

            fn $bin_op_fn(mut self, rhs: &'_ LocSet) -> Self::Output {
                self.$bin_assign_op_fn(rhs);
                self
            }
        }

        impl $bin_op<LocSet> for LocSet {
            type Output = LocSet;

            fn $bin_op_fn(mut self, rhs: LocSet) -> Self::Output {
                self.$bin_assign_op_fn(rhs);
                self
            }
        }

        impl $bin_op<LocSet> for &'_ LocSet {
            type Output = LocSet;

            fn $bin_op_fn(self, mut rhs: LocSet) -> Self::Output {
                rhs.$bin_assign_rev_op_fn(self);
                rhs
            }
        }

        const _: fn() = {
            fn _check<T>()
            where
                for<'a> T: $bin_op<T> + $bin_op<&'a T> + $bin_assign_op<T> + $bin_assign_op<&'a T>,
                for<'a, 'b> &'a T: $bin_op<T> + $bin_op<&'b T>,
            {
            }
            _check::<LocSet>
        };
    };
}

impl BitAnd<&'_ LocSet> for &'_ LocSet {
    type Output = LocSet;

    fn bitand(self, rhs: &'_ LocSet) -> Self::Output {
        LocSet::from_starts_map_iter_unchecked(self.starts_map.iter().map(|(&reg_len, starts)| {
            (
                reg_len,
                enum_map! {kind => (&starts[kind]).bitand(rhs.starts(reg_len, kind))},
            )
        }))
    }
}

impl BitAndAssign<&'_ LocSet> for LocSet {
    fn bitand_assign(&mut self, rhs: &'_ LocSet) {
        self.for_each_reg_len_filtering_out_empty_entries(|reg_len, starts| {
            for (kind, starts) in starts {
                starts.bitand_assign(rhs.starts(reg_len, kind));
            }
        });
    }
}

/// helper for binary operations that keeps Locs not present in rhs
macro_rules! impl_bin_op_keep {
    (
        $bin_op:ident::$bin_op_fn:ident(),
        $bin_assign_op:ident::$bin_assign_op_fn:ident(),
    ) => {
        impl $bin_op<&'_ LocSet> for &'_ LocSet {
            type Output = LocSet;

            fn $bin_op_fn(self, rhs: &'_ LocSet) -> Self::Output {
                let mut retval: LocSet = self.clone();
                retval.$bin_assign_op_fn(rhs);
                retval
            }
        }

        impl $bin_assign_op<&'_ LocSet> for LocSet {
            fn $bin_assign_op_fn(&mut self, rhs: &'_ LocSet) {
                self.bin_op_keep_helper(rhs, |_reg_len, lhs_starts, rhs_starts| {
                    for (kind, rhs_starts) in rhs_starts {
                        lhs_starts[kind].$bin_assign_op_fn(rhs_starts);
                    }
                });
            }
        }
    };
}

forward_bin_op! {
    BitAnd::bitand(),
    BitAndAssign::bitand_assign(),
    bitand_assign(),
}

impl_bin_op_keep! {
    BitOr::bitor(),
    BitOrAssign::bitor_assign(),
}

forward_bin_op! {
    BitOr::bitor(),
    BitOrAssign::bitor_assign(),
    bitor_assign(),
}

impl_bin_op_keep! {
    BitXor::bitxor(),
    BitXorAssign::bitxor_assign(),
}

forward_bin_op! {
    BitXor::bitxor(),
    BitXorAssign::bitxor_assign(),
    bitxor_assign(),
}

impl Sub<&'_ LocSet> for &'_ LocSet {
    type Output = LocSet;

    fn sub(self, rhs: &'_ LocSet) -> Self::Output {
        LocSet::from_starts_map_iter_unchecked(self.starts_map.iter().map(|(&reg_len, starts)| {
            (
                reg_len,
                enum_map! {kind => and_not(&starts[kind], rhs.starts(reg_len, kind))},
            )
        }))
    }
}

impl SubAssign<&'_ LocSet> for LocSet {
    fn sub_assign(&mut self, rhs: &'_ LocSet) {
        self.bin_op_keep_helper(rhs, |_reg_len, lhs_starts, rhs_starts| {
            for (kind, lhs_starts) in lhs_starts {
                let rhs_starts = &rhs_starts[kind];
                if rhs_starts.is_zero() {
                    continue;
                }
                *lhs_starts = and_not(mem::take(lhs_starts), rhs_starts);
            }
        });
    }
}

forward_bin_op! {
    Sub::sub(),
    SubAssign::sub_assign(),
    sub_reverse_assign(),
}

/// the largest number of Locs in `lhs` that a single Loc
/// from `rhs` can conflict with
#[derive(Clone)]
pub struct LocSetMaxConflictsWith<Rhs> {
    lhs: Interned<LocSet>,
    rhs: Rhs,
    // result is not included in equality or hash
    result: Cell<Option<u32>>,
}

impl<Rhs: Hash> Hash for LocSetMaxConflictsWith<Rhs> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.lhs.hash(state);
        self.rhs.hash(state);
    }
}

impl<Rhs: Eq> Eq for LocSetMaxConflictsWith<Rhs> {}

impl<Rhs: PartialEq> PartialEq for LocSetMaxConflictsWith<Rhs> {
    fn eq(&self, other: &Self) -> bool {
        self.lhs == other.lhs && self.rhs == other.rhs
    }
}

pub trait LocSetMaxConflictsWithTrait: Clone {
    fn intern(
        v: LocSetMaxConflictsWith<Self>,
        global_state: &GlobalState,
    ) -> Interned<LocSetMaxConflictsWith<Self>>;
    fn compute_result(lhs: &Interned<LocSet>, rhs: &Self, global_state: &GlobalState) -> u32;
    #[cfg(feature = "fuzzing")]
    fn reference_compute_result(
        lhs: &Interned<LocSet>,
        rhs: &Self,
        global_state: &GlobalState,
    ) -> u32;
}

impl LocSetMaxConflictsWithTrait for Loc {
    fn compute_result(lhs: &Interned<LocSet>, rhs: &Self, _global_state: &GlobalState) -> u32 {
        // now we do the equivalent of:
        // return lhs.iter().map(|loc| rhs.conflicts(loc) as u32).sum()
        let mut retval = 0;
        for (&lhs_reg_len, lhs_starts) in lhs.starts_map() {
            let lhs_starts = &lhs_starts[rhs.kind];
            if lhs_starts.is_zero() {
                continue;
            }
            // now we do the equivalent of:
            // retval += sum(rhs.start < lhs_start + lhs_reg_len
            //               and lhs_start < rhs.start + rhs.reg_len
            //               for lhs_start in lhs_starts)
            let lhs_stops = lhs_starts << lhs_reg_len.get();

            // find all the bit indexes `i` where `i < rhs.start + 1`
            let lt_rhs_start_plus_1 = (BigUint::from(1u32) << (rhs.start + 1)) - 1u32;

            // find all the bit indexes `i` where
            // `i < rhs.start + rhs.reg_len + lhs_reg_len`
            let lt_rhs_start_plus_rhs_reg_len_plus_reg_len =
                (BigUint::from(1u32) << (rhs.start + rhs.reg_len.get() + lhs_reg_len.get())) - 1u32;
            let lhs_stops_and_lt_rhs_start_plus_1 = &lhs_stops & lt_rhs_start_plus_1;
            let mut included = and_not(lhs_stops, lhs_stops_and_lt_rhs_start_plus_1);
            included &= lt_rhs_start_plus_rhs_reg_len_plus_reg_len;
            retval += included.count_ones() as u32;
        }
        retval
    }

    #[cfg(feature = "fuzzing")]
    fn reference_compute_result(
        lhs: &Interned<LocSet>,
        rhs: &Self,
        _global_state: &GlobalState,
    ) -> u32 {
        lhs.iter().map(|loc| rhs.conflicts(loc) as u32).sum::<u32>()
    }

    fn intern(
        v: LocSetMaxConflictsWith<Self>,
        global_state: &GlobalState,
    ) -> Interned<LocSetMaxConflictsWith<Self>> {
        v.into_interned(global_state)
    }
}

impl LocSetMaxConflictsWithTrait for Interned<LocSet> {
    fn compute_result(lhs: &Interned<LocSet>, rhs: &Self, global_state: &GlobalState) -> u32 {
        rhs.iter()
            .map(|loc| lhs.clone().max_conflicts_with(loc, global_state))
            .max()
            .unwrap_or(0)
    }

    #[cfg(feature = "fuzzing")]
    fn reference_compute_result(
        lhs: &Interned<LocSet>,
        rhs: &Self,
        global_state: &GlobalState,
    ) -> u32 {
        rhs.iter()
            .map(|loc| lhs.clone().reference_max_conflicts_with(loc, global_state))
            .max()
            .unwrap_or(0)
    }

    fn intern(
        v: LocSetMaxConflictsWith<Self>,
        global_state: &GlobalState,
    ) -> Interned<LocSetMaxConflictsWith<Self>> {
        v.into_interned(global_state)
    }
}

impl<Rhs: LocSetMaxConflictsWithTrait> LocSetMaxConflictsWith<Rhs> {
    pub fn lhs(&self) -> &Interned<LocSet> {
        &self.lhs
    }
    pub fn rhs(&self) -> &Rhs {
        &self.rhs
    }
    pub fn result(&self, global_state: &GlobalState) -> u32 {
        match self.result.get() {
            Some(v) => v,
            None => {
                let retval = Rhs::compute_result(&self.lhs, &self.rhs, global_state);
                self.result.set(Some(retval));
                retval
            }
        }
    }
    #[cfg(feature = "fuzzing")]
    pub fn reference_result(&self, global_state: &GlobalState) -> u32 {
        match self.result.get() {
            Some(v) => v,
            None => {
                let retval = Rhs::reference_compute_result(&self.lhs, &self.rhs, global_state);
                self.result.set(Some(retval));
                retval
            }
        }
    }
}

impl Interned<LocSet> {
    pub fn max_conflicts_with<Rhs>(self, rhs: Rhs, global_state: &GlobalState) -> u32
    where
        Rhs: LocSetMaxConflictsWithTrait,
        LocSetMaxConflictsWith<Rhs>: InternTarget,
    {
        LocSetMaxConflictsWithTrait::intern(
            LocSetMaxConflictsWith {
                lhs: self,
                rhs,
                result: Cell::default(),
            },
            global_state,
        )
        .result(global_state)
    }
    #[cfg(feature = "fuzzing")]
    pub fn reference_max_conflicts_with<Rhs>(self, rhs: Rhs, global_state: &GlobalState) -> u32
    where
        Rhs: LocSetMaxConflictsWithTrait,
        LocSetMaxConflictsWith<Rhs>: InternTarget,
    {
        LocSetMaxConflictsWithTrait::intern(
            LocSetMaxConflictsWith {
                lhs: self,
                rhs,
                result: Cell::default(),
            },
            global_state,
        )
        .reference_result(global_state)
    }
    pub fn conflicts_with<Rhs>(self, rhs: Rhs, global_state: &GlobalState) -> bool
    where
        Rhs: LocSetMaxConflictsWithTrait,
        LocSetMaxConflictsWith<Rhs>: InternTarget,
    {
        self.max_conflicts_with(rhs, global_state) != 0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_and_not() {
        for a in 0..0x10u32 {
            for b in 0..0x10 {
                assert_eq!(
                    and_not(&BigUint::from(a), BigUint::from(b)),
                    (a & !b).into()
                );
            }
        }
    }
}
