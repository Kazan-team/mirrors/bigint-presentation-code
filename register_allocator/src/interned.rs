use crate::{
    loc::Loc,
    loc_set::{LocSet, LocSetMaxConflictsWith},
};
use hashbrown::{
    hash_map::{Entry, RawEntryMut},
    HashMap,
};
use serde::{de, Deserialize, Serialize};
use std::{
    cell::RefCell,
    cmp::Ordering,
    fmt,
    hash::{Hash, Hasher},
    ops::Deref,
    rc::Rc,
};

pub struct Interned<T: ?Sized> {
    ptr: Rc<T>,
}

impl<T: ?Sized> Deref for Interned<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.ptr.deref()
    }
}

impl<T: ?Sized> Clone for Interned<T> {
    fn clone(&self) -> Self {
        Self {
            ptr: self.ptr.clone(),
        }
    }
}

impl<T: ?Sized> Hash for Interned<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        Rc::as_ptr(&self.ptr).hash(state);
    }
}

impl<T: ?Sized> Eq for Interned<T> {}

impl<T: ?Sized + fmt::Debug> fmt::Debug for Interned<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.ptr.fmt(f)
    }
}

impl<T: ?Sized + fmt::Display> fmt::Display for Interned<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.ptr.fmt(f)
    }
}

pub struct SerdeState {
    global_state: Rc<GlobalState>,
    inner: SerdeStateInner,
}

scoped_tls::scoped_thread_local!(static SERDE_STATE: SerdeState);

impl SerdeState {
    pub fn global_state(&self) -> &Rc<GlobalState> {
        &self.global_state
    }
    #[cold]
    pub fn scope<R>(global_state: &Rc<GlobalState>, f: impl FnOnce() -> R) -> R {
        SERDE_STATE.set(
            &SerdeState {
                global_state: global_state.clone(),
                inner: SerdeStateInner::default(),
            },
            f,
        )
    }
    pub fn get_or_scope<R>(f: impl for<'a> FnOnce(&'a SerdeState) -> R) -> R {
        if SERDE_STATE.is_set() {
            SERDE_STATE.with(f)
        } else {
            GlobalState::get(|global_state| Self::scope(global_state, || SERDE_STATE.with(f)))
        }
    }
}

pub struct SerdeStateFor<T: ?Sized> {
    de: RefCell<Vec<Interned<T>>>,
    ser: RefCell<HashMap<Interned<T>, usize>>,
}

impl<T: ?Sized> Default for SerdeStateFor<T> {
    fn default() -> Self {
        Self {
            de: Default::default(),
            ser: Default::default(),
        }
    }
}

#[derive(Serialize, Deserialize)]
enum SerializedInterned<T> {
    Old(usize),
    New(T),
}

impl<T: ?Sized + Serialize + InternTarget> Serialize for Interned<T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        SerdeState::get_or_scope(|serde_state| {
            let mut state = T::get_serde_state_for(serde_state).ser.borrow_mut();
            let next_index = state.len();
            match state.entry(self.clone()) {
                Entry::Occupied(entry) => SerializedInterned::Old(*entry.get()),
                Entry::Vacant(entry) => {
                    entry.insert(next_index);
                    SerializedInterned::<&T>::New(self)
                }
            }
            .serialize(serializer)
        })
    }
}

impl<'de, T, Owned> Deserialize<'de> for Interned<T>
where
    T: ?Sized + InternTarget + ToOwned<Owned = Owned>,
    Owned: Deserialize<'de> + Intern<Target = T>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        SerdeState::get_or_scope(|serde_state| {
            let mut state = T::get_serde_state_for(serde_state).de.borrow_mut();
            match SerializedInterned::<Owned>::deserialize(deserializer)? {
                SerializedInterned::Old(index) => state
                    .get(index)
                    .cloned()
                    .ok_or_else(|| <D::Error as de::Error>::custom("index out of range")),
                SerializedInterned::New(value) => {
                    let retval = value.into_interned(&serde_state.global_state);
                    state.push(retval.clone());
                    Ok(retval)
                }
            }
        })
    }
}

impl<T: ?Sized> PartialEq for Interned<T> {
    fn eq(&self, other: &Interned<T>) -> bool {
        Rc::ptr_eq(&self.ptr, &other.ptr)
    }
}

impl<T: ?Sized> PartialOrd for Interned<T> {
    fn partial_cmp(&self, other: &Interned<T>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<T: ?Sized> Ord for Interned<T> {
    fn cmp(&self, other: &Interned<T>) -> Ordering {
        Rc::as_ptr(&self.ptr).cmp(&Rc::as_ptr(&other.ptr))
    }
}

macro_rules! make_interners {
    {
        $(
            $(#[$impl_intern_target:ident intern_target])?
            $name:ident: $ty:ty,
        )*
    } => {
        #[derive(Default)]
        struct Interners {
            $($name: Interner<$ty>,)*
        }

        #[derive(Default)]
        struct SerdeStateInner {
            $($name: SerdeStateFor<$ty>,)*
        }

        $($(
            $impl_intern_target InternTarget for $ty {
                fn get_interner(global_state: &GlobalState) -> &Interner<Self> {
                    &global_state.interners.$name
                }

                fn get_serde_state_for(serde_state: &SerdeState) -> &SerdeStateFor<Self> {
                    &serde_state.inner.$name
                }
            }
        )?)*
    };
}

make_interners! {
    str: str,
    #[impl intern_target]
    loc_set: LocSet,
    #[impl intern_target]
    loc_set_max_conflicts_with_loc_set: LocSetMaxConflictsWith<Interned<LocSet>>,
    #[impl intern_target]
    loc_set_max_conflicts_with_loc: LocSetMaxConflictsWith<Loc>,
}

pub struct GlobalState {
    interners: Interners,
}

impl fmt::Debug for GlobalState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("GlobalState").finish_non_exhaustive()
    }
}

scoped_tls::scoped_thread_local!(static GLOBAL_STATE: Rc<GlobalState>);

impl GlobalState {
    #[cold]
    pub fn scope<R>(f: impl FnOnce() -> R) -> R {
        GLOBAL_STATE.set(
            &Rc::new(GlobalState {
                interners: Interners::default(),
            }),
            f,
        )
    }
    pub fn get<R>(f: impl for<'a> FnOnce(&'a Rc<GlobalState>) -> R) -> R {
        GLOBAL_STATE.with(f)
    }
}

pub struct Interner<T: ?Sized>(RefCell<HashMap<Rc<T>, ()>>);

impl<T: ?Sized> Default for Interner<T> {
    fn default() -> Self {
        Self(Default::default())
    }
}

pub struct InternInput<
    Input,
    T: ?Sized + Eq + Hash,
    B: FnOnce(&Input) -> &T,
    R: FnOnce(Input) -> Rc<T>,
> {
    input: Input,
    borrow: B,
    into_rc: R,
}

impl<T: ?Sized + Eq + Hash> Interner<T> {
    fn intern<Input>(
        &self,
        v: InternInput<Input, T, impl FnOnce(&Input) -> &T, impl FnOnce(Input) -> Rc<T>>,
    ) -> Interned<T> {
        let InternInput {
            input,
            borrow,
            into_rc,
        } = v;
        match self.0.borrow_mut().raw_entry_mut().from_key(borrow(&input)) {
            RawEntryMut::Occupied(entry) => Interned {
                ptr: entry.key().clone(),
            },
            RawEntryMut::Vacant(entry) => Interned {
                ptr: entry.insert(into_rc(input), ()).0.clone(),
            },
        }
    }
}

pub trait InternTarget: Intern<Target = Self> + Hash + Eq {
    fn get_interner(global_state: &GlobalState) -> &Interner<Self>;
    fn get_serde_state_for(serde_state: &SerdeState) -> &SerdeStateFor<Self>;
    fn into_interned(input: Self, global_state: &GlobalState) -> Interned<Self>
    where
        Self: Sized,
    {
        Self::get_interner(global_state).intern(InternInput {
            input,
            borrow: |v| v,
            into_rc: Rc::new,
        })
    }
    fn rc_into_interned(input: Rc<Self>, global_state: &GlobalState) -> Interned<Self> {
        Self::get_interner(global_state).intern(InternInput {
            input,
            borrow: |v| &**v,
            into_rc: |v| v,
        })
    }
    fn rc_to_interned(input: &Rc<Self>, global_state: &GlobalState) -> Interned<Self> {
        Self::get_interner(global_state).intern(InternInput {
            input,
            borrow: |v| &***v,
            into_rc: |v| v.clone(),
        })
    }
}

impl<'a, T: arbitrary::Arbitrary<'a> + InternTarget> arbitrary::Arbitrary<'a> for Interned<T> {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        let retval: T = u.arbitrary()?;
        Ok(GlobalState::get(|global_state| {
            retval.into_interned(global_state)
        }))
    }
}

impl InternTarget for str {
    fn get_interner(global_state: &GlobalState) -> &Interner<Self> {
        &global_state.interners.str
    }
    fn get_serde_state_for(serde_state: &SerdeState) -> &SerdeStateFor<Self> {
        &serde_state.inner.str
    }
}

impl Intern for str {
    type Target = str;

    fn to_rc_target(v: &Self) -> Rc<Self::Target> {
        v.into()
    }

    fn to_interned(&self, global_state: &GlobalState) -> Interned<Self::Target> {
        Self::get_interner(global_state).intern(InternInput {
            input: self,
            borrow: |v| &**v,
            into_rc: Self::to_rc_target,
        })
    }
}

impl Intern for &'_ str {
    type Target = str;

    fn to_rc_target(v: &Self) -> Rc<Self::Target> {
        Rc::from(*v)
    }

    fn to_interned(&self, global_state: &GlobalState) -> Interned<Self::Target> {
        Self::Target::to_interned(self, global_state)
    }

    fn into_interned(self, global_state: &GlobalState) -> Interned<Self::Target>
    where
        Self: Sized,
    {
        Self::Target::to_interned(self, global_state)
    }
}

impl Intern for &'_ mut str {
    type Target = str;

    fn to_rc_target(v: &Self) -> Rc<Self::Target> {
        Rc::from(&**v)
    }

    fn to_interned(&self, global_state: &GlobalState) -> Interned<Self::Target> {
        Self::Target::to_interned(self, global_state)
    }

    fn into_interned(self, global_state: &GlobalState) -> Interned<Self::Target>
    where
        Self: Sized,
    {
        Self::Target::to_interned(self, global_state)
    }
}

impl Intern for String {
    type Target = str;

    fn to_rc_target(v: &Self) -> Rc<Self::Target> {
        Rc::from(&**v)
    }

    fn to_interned(&self, global_state: &GlobalState) -> Interned<Self::Target> {
        Self::Target::to_interned(self, global_state)
    }

    fn into_rc_target(v: Self) -> Rc<Self::Target>
    where
        Self: Sized,
    {
        Rc::from(v)
    }

    fn into_interned(self, global_state: &GlobalState) -> Interned<Self::Target>
    where
        Self: Sized,
    {
        Self::Target::to_interned(&self, global_state)
    }
}

pub trait Intern {
    type Target: ?Sized + InternTarget;
    fn into_rc_target(v: Self) -> Rc<Self::Target>
    where
        Self: Sized,
    {
        Self::to_rc_target(&v)
    }
    fn to_rc_target(v: &Self) -> Rc<Self::Target>;
    fn into_interned(self, global_state: &GlobalState) -> Interned<Self::Target>
    where
        Self: Sized,
    {
        <<Self as Intern>::Target as InternTarget>::rc_into_interned(
            Self::into_rc_target(self),
            global_state,
        )
    }
    fn to_interned(&self, global_state: &GlobalState) -> Interned<Self::Target> {
        Self::Target::rc_into_interned(Self::to_rc_target(self), global_state)
    }
}

impl<T: ?Sized + InternTarget> Intern for Rc<T> {
    type Target = T;

    fn to_rc_target(v: &Self) -> Rc<Self::Target> {
        v.clone()
    }

    fn into_rc_target(v: Self) -> Rc<Self::Target>
    where
        Self: Sized,
    {
        v
    }
}

impl<T: Clone + InternTarget> Intern for T {
    type Target = T;

    fn to_rc_target(v: &Self) -> Rc<Self::Target> {
        v.clone().into()
    }

    fn into_rc_target(v: Self) -> Rc<Self::Target>
    where
        Self: Sized,
    {
        v.into()
    }

    fn into_interned(self, global_state: &GlobalState) -> Interned<Self::Target>
    where
        Self: Sized,
    {
        InternTarget::into_interned(self, global_state)
    }

    fn to_interned(&self, global_state: &GlobalState) -> Interned<Self::Target> {
        InternTarget::get_interner(global_state).intern(InternInput {
            input: self,
            borrow: |v| &**v,
            into_rc: |v| v.clone().into(),
        })
    }
}
